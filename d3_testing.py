
import numpy as np


def calculate_rms(data_array):
    """
    Calculate rsam of a data array 
    
    """
    data_np64=np.array(data_array,dtype='float64')
    square_data_np64=np.square(data_np64)
    sum_data=square_data_np64.sum()
    media_data=sum_data/len(data_array)
    rms=media_data**(0.5)
    return rms  



def calculate_rms_fail(data_array):
    """
    Calculate rsam of a data array 
    
    """
    data_np64=np.array(data_array,dtype='float64')
    square_data_np64=np.square(data_np64)
    sum_data=square_data_np64.sum()
    media_data=sum_data/len(data_array)
    rms=media_data*(0.5)
    print(rms)
    return rms