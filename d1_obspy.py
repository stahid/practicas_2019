from obspy import UTCDateTime
from obspy.clients.arclink import Client as cliente_arclink 
from obspy import read
import d1_function as dia1_funcion
import d1_obspy_function

#####
"""READ LOCAL MSEED FILE

mseed_file="./data/aped.20160416.mseed"

mseed_stream=read(mseed_file)

mseed_stream.plot()

mseed_data=mseed_stream[0].data

rms=dia1_funcion.calculate_rsam(mseed_data)

print(rms)

"""

#####

"""READ MSEED FROM SERVER"""



diaUTC=UTCDateTime("2016-04-16 19:08:20")

cliente_datos=cliente_arclink('usuario','192.168.131.2',18001)

data_stream=cliente_datos.get_waveforms('EC','APED','','HNZ',diaUTC,diaUTC+60,route=False,compressed=False)

#data_stream.plot()
data_stream.plot(outfile="./data/data_plot.png")


####

"""CALL BASIC OBSPY FUNCTION
diaUTC=UTCDateTime("2019-06-11 00:00:00")
data_window=3600
station={"net":"EC","cod":"BMOR","loc":"","cha":"BHZ"}

data=d1_obspy_function.get_data(station['net'], station['cod'], station['loc'], station['cha'], diaUTC, diaUTC+data_window)

data.plot()
"""

###


"""CALL BASIC OBSPY FUNCTION N TIMES

diaUTC=UTCDateTime("2019-06-12 00:00:00")
data_window=86400

station_list=[{"net":"EC","cod":"BREF","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BVC2","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BMOR","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BTAM","loc":"","cha":"BHZ"}
              ]
              
for station in station_list:
    print("Get data and plot for:%s" %station['cod'])
    data=d1_obspy_function.get_data(station['net'], station['cod'], station['loc'], station['cha'], diaUTC, diaUTC+data_window)
    data.plot(outfile="./data/%s.png" %station['cod'])

"""

###


"""CALL BASIC OBSPY FUNCTION N TIMES: WILL RAISE AN ERROR

diaUTC=UTCDateTime("2019-06-12 00:00:00")
data_window=86400

station_list=[{"net":"EC","cod":"BREF","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BVC2","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BMOR","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BNAS","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BTAM","loc":"","cha":"BHZ"}
              ]
              
for station in station_list:
    print("Get data and plot for:%s" %station['cod'])
    data=d1_obspy_function.get_data(station['net'], station['cod'], station['loc'], station['cha'], diaUTC, diaUTC+data_window)
    data.plot(outfile="./data/%s.png" %station['cod'])
"""

###


"""CALL OBSPY FUNCTION N TIMES: CORRECTED """ 


diaUTC=UTCDateTime("2019-06-12 00:00:00")
data_window=86400

station_list=[{"net":"EC","cod":"BREF","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BVC2","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BNAS","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BMOR","loc":"","cha":"BHZ"},
              {"net":"EC","cod":"BTAM","loc":"","cha":"BHZ"}
              ]
              
for station in station_list:
    
    print("Get data and plot for:%s" %station['cod'])
    data=d1_obspy_function.get_data_v2(station['net'], station['cod'], station['loc'], station['cha'], diaUTC, diaUTC+data_window)
    if data !=None  :
        print(data)
        data.plot(outfile="./data/%s.png" %station['cod'])
    else:
        print("No data found for:%s" %station['cod'])
    














