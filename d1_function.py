
import numpy as np
from matplotlib import pyplot as plt

def basic_plot(axis_x,axis_y):
    
    plt.plot(axis_x,axis_y)
    plt.show()
    
    
def title_plot(axis_x,axis_y,title):
    plt.title(title)
    plt.plot(axis_x,axis_y)
    plt.show()



def calculate_rsam(data_array):
    """
    Calculate rsam of a data array 
    
    """
    data_np64=np.array(data_array,dtype='float64')
    square_data_np64=np.square(data_np64)
    sum_data=square_data_np64.sum()
    media_data=sum_data/len(data_array)
    rms=media_data**(0.5)
    return rms   



