"""
1era modificacion: cambiar el nombre del usuario por el propio: 
git status 
git add d3_git.py
git commit 

2da modificacion: agregar el parametro nombre_usuario a funcion_original
3ra modificacion: renombrar funcion_original a imprimir_nombre
"""

def funcion_original():
    
    usuario="wacero@igepn.edu.ec"
    mensaje="Modificado por el usuario: %s" %usuario
    
    print(mensaje)
    

def nueva_funcion():
    print("Suma: %s" %(2+2) )


funcion_original()

nueva_funcion()

