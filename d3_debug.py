
import numpy as np
import logging
import os


from matplotlib import pyplot as plt


lista_array_ok=([0., 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1,0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2 , 0.21 ],\
            [0.  , 0.01, 0.02, 0.03, 0.04, 0.06, 0.07, 0.08, 0.09, 0.1,],\
            [0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5 , 0.51, 0.52, 0.53, 0.54])


lista_array_f=([0., 0.01, 0.02, 0.03, 0.04, 0.06, 0.07, 0.08, 0.09, 0.1,],\
               [0., 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1,0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2 , 0.21, 0.3,0.7,'w' ],\
               [0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5 , 0.51, 0.52, 0.53, 0.54])

def calculate_square(data_array):
   
    plt.plot(np.square(data_array))
    plt.show()


def calculate_square_log(data_array):
    logging.info("Started function calculate_square_log" )

    try:
        logging.info("Raise to square and plot the array: %s" %data_array)
        plt.plot(np.square(data_array))
        plt.show()
    except Exception as e:
        logging.error("Error in calculate_square_log, data: %s, error: %s" %(data_array,str(e)))



def debug_ok():
    
    for data_array in lista_array_ok:
        calculate_square(data_array)

def debug_fail():  
    """
    Para usar el debug:
    cd PATH_A_ESTE_DIRECTORIO
    $ python
    >>> import pdb 
    >>> import d3_test_debug
    >>> pdb.runcall(d3_test_debug.debug_test_1)
    
    """
    for data_array in lista_array_f:
        calculate_square(data_array)

def debug_print():
    for data_array in lista_array_f:
        print("DATOS A PROCESAR: %s " %data_array)
        calculate_square(data_array)

def debug_log():
    
    exec_dir=os.path.realpath(os.path.dirname(__file__))
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',filename=os.path.join(exec_dir,"practicas_2019.log"),level=logging.ERROR)
    
    
    for data_array in lista_array_f:
        logging.debug("Inputa array: %s" %data_array)
        calculate_square_log(data_array)
        
        
        
        
        
        